//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <plane.h>

uint
qHash(rect_t rect)
{
    int32_t x0 = rect.topLeft().x();
    int32_t y0 = rect.topLeft().y();

    int32_t x1 = rect.bottomRight().x();
    int32_t y1 = rect.bottomRight().y();

    uint64_t i1 = ((uint64_t) x0 << 32) | (x1);
    uint64_t i2 = ((uint64_t) y0 << 32) | (y1);

    return qHash(qHash(i1) ^ qHash(i2));
}

plane_t::plane_t(rect_t rect, point_t plane_origin, std::shared_ptr<plane_t> parent) : m_plane(rect), m_plane_origin(plane_origin), m_parent(parent) {}

rect_t
plane_t::parent_rect()
{
    return rect_t(origin(), rect().size());
}

rect_t
plane_t::rect()
{
    return m_plane.boundingRect();
}

void
plane_t::set_rect(rect_t rect)
{
    m_plane &= rect_t();

    m_plane += rect;
    set_updated(true);
}

point_t
plane_t::origin()
{
    return m_plane_origin;
}

void
plane_t::operator+=(const rect_t &rect)
{
    m_plane += rect;
    set_updated(true);
}

void
plane_t::operator-=(const rect_t &rect)
{
    m_plane -= rect;
    set_updated(true);
}

void
plane_t::set_origin(const point_t &point)
{
    m_plane_origin = point;
    set_updated(true);
}

void
plane_t::reset_plane()
{
    m_plane = region_t();
    set_updated(true);
}

transform_t
plane_t::translate(plane_t &plane)
{
    transform_t transform;
    qreal sx = plane.origin().x();
    qreal sy = plane.origin().y();

    transform.translate(sx, sy);

    return transform;
}

transform_t
plane_t::from(plane_t &plane)
{
    return translate(plane);
}

transform_t
plane_t::to(plane_t &plane)
{
    return from(plane).inverted();
}

point_t
plane_t::map_to(plane_t *dest_plane, point_t point)
{
    if (!dest_plane) {
        return point_t(0, 0);
    }

    if ((this != dest_plane->m_parent.get() && m_parent.get() != dest_plane) && (m_parent.get() != dest_plane->m_parent.get())) {

        vg_debug() << DTRACE << "planes do not have a valid relationship";
        return point_t(0, 0);
    }

    return to(*dest_plane).map(point);
}

point_t
plane_t::map_from(plane_t *src_plane, point_t point)
{
    if (!src_plane) {
        return point_t(0, 0);
    }

    if ((this != src_plane->m_parent.get() && m_parent.get() != src_plane) && (m_parent.get() != src_plane->m_parent.get())) {

        vg_debug() << DTRACE << "planes do not have a valid relationship";
        return point_t(0, 0);
    }

    return from(*src_plane).map(point);
}

rect_t
plane_t::map_to(plane_t *dest_plane, rect_t rect)
{
    if (!dest_plane) {
        return rect_t(0, 0, 0, 0);
    }

    return to(*dest_plane).mapRect(rect);
}

rect_t
plane_t::map_from(plane_t *src_plane, rect_t rect)
{
    Expects(src_plane);

    return from(*src_plane).mapRect(rect);
}

region_t
plane_t::map_to(plane_t *dest_plane, region_t region)
{
    if (!dest_plane) {
        return region_t();
    }

    return to(*dest_plane).map(region);
}

region_t
plane_t::map_from(plane_t *src_plane, region_t region)
{
    Expects(src_plane);

    return from(*src_plane).map(region);
}
