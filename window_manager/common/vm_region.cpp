//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <vm_region.h>

int
dump_region(region_t region)
{
    vg_info() << region;
    return 0;
}

// Visible region is in the desktop coordinate space already, so
// make calculations accordingly.
region_t
vm_region_t::create_visible_region(desktop_plane_t *desktop,
                                   display_plane_t *display_plane,
                                   render_target_plane_t *rtp)
{
    (void) rtp;
    Expects(display_plane);

    region_t desktop_region = m_visible_region & display_plane->parent_rect();
    region_t clip_region = desktop->map_to(display_plane,
                                           desktop_region);

    if (0) {
        dump_region(clip_region);
    }

    return clip_region;
}

// The dirty region is still in the guest coordinate space, so
// it requires a few extra steps of translation before it's ready
// for renderer consumption.
region_t
vm_region_t::create_dirty_region(desktop_plane_t *desktop,
                                 display_plane_t *display_plane,
                                 render_target_plane_t *rtp)
{
    Expects(display_plane);

    if (!rtp->render_source()) {
        return desktop->map_to(display_plane, rtp->parent_rect());
    }

    region_t render_target_region = rtp->map_from(rtp->render_source(),
                                                  m_dirty_region);
    region_t desktop_region = desktop->map_from(rtp,
                                                render_target_region);
    region_t clip_region = desktop->map_to(display_plane,
                                           desktop_region);

    return clip_region & desktop->map_to(display_plane, rtp->parent_rect());
}

void
vm_region_t::render_overlays(QPainter &painter, desktop_plane_t *desktop, display_plane_t *display_plane, render_target_plane_t *rtp, region_t &display_clip, region_t &painted_clip)
{
    (void) rtp;

    for (auto overlay : m_overlays) {
        if (!overlay) {
            continue;
        }

        if (overlay->region().intersects(display_plane->parent_rect())) {
            overlay->render_overlay(painter, desktop, display_plane, display_clip, painted_clip);
        }
    }
}
