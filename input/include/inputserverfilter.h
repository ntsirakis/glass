//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUTSERVER_FILTER__H
#define INPUTSERVER_FILTER__H

#include <inputaction.h>
#include <json.hpp>
#include <sources/libinputsource.h>
#include <vm.h>
#include <xt_input_global.h>

using json = nlohmann::json;

class input_server_filter_t : public QObject
{
    Q_OBJECT

public:
    input_server_filter_t();
    virtual ~input_server_filter_t(void);

    virtual bool filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event) = 0;

    virtual bool set_triggers(const QString triggers_str)
    {
        (void) triggers_str;
        return false;
    }
    virtual bool clear_triggers() { return false; }
    virtual bool add_trigger(const json trigger)
    {
        (void) trigger;
        return false;
    }

    void set_name(QString s);
    QString get_name();

signals:

    void filter_fire(const std::shared_ptr<input_action_t> &action);
    void idle_timeout(const QString &idle_name);
    void input_detected();

public slots:

    virtual void update_timeout(QString timer_name, int32_t timeout);

private:
    QString m_name;
};

#endif // INPUTSERVER_FILTER__H
