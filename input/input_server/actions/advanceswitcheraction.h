//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef ADVANCE_SWITCHER_ACTION__H
#define ADVANCE_SWITCHER_ACTION__H

#include <inputaction.h>

class advance_switcher_action_t : public input_action_t
{
    Q_OBJECT

public:
    advance_switcher_action_t(void);
    ~advance_switcher_action_t(void);

    void operator()();

signals:

    void advance_switcher(void);
};

#endif //ADVANCE_SWITCHER_ACTION__H
