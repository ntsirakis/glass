//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <showswitchaction.h>

show_switch_action_t::show_switch_action_t() : m_highlight_vm(uuid_t())
{
}

show_switch_action_t::~show_switch_action_t(void)
{
}

void
show_switch_action_t::set_highlight_vm(uuid_t highlight)
{
    m_highlight_vm = highlight;
    emit highlighted_vm_changed(m_highlight_vm);
}

void
show_switch_action_t::advance_highlight_vm_right(void)
{
    emit advance_vm_right(m_highlight_vm);
}

void
show_switch_action_t::advance_highlight_vm_left(void)
{
    emit advance_vm_left(m_highlight_vm);
}

void
show_switch_action_t::reset_switcher(uuid_t vm)
{
    (void) vm;
}

void
show_switch_action_t::operator()()
{
    emit show_vm_switcher();
}
