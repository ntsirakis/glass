//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <QThread>
#include <idlefilter.h>

/**
 * Create a new filter to detect long periods of idleness.
 */
idle_filter_t::idle_filter_t() : m_signal_mapper(std::make_shared<QSignalMapper>())
{
    connect(m_signal_mapper.get(), SIGNAL(mapped(QString)), this, SLOT(timed_out(QString)));

    this->set_name("idle_filter");
}

idle_filter_t::~idle_filter_t(void)
{
}

/**
 * Perform our actual event filtering.
 */
bool
idle_filter_t::filter_event(std::shared_ptr<vm_input_t> &, xt_input_event *)
{
    for (auto &timer_set : m_triggers) {
        if (timer_set.mTimeout > 0) {
            timer_set.mTimer->start(timer_set.mTimeout * 1000);
        }
    }

    return false;
}

void
idle_filter_t::idle_timer_update(QString timer_name, int32_t timeout)
{
    for (auto &timer_set : m_triggers) {
        if (timer_set.mTimerName == timer_name) {
            timer_set.mTimeout = timeout;
            if (timeout > 0) {
                timer_set.mTimer->start(timeout * 1000);
            } else {
                timer_set.mTimer->stop();
            }
        }
    }
}

void
idle_filter_t::timed_out(const QString &timer_name)
{
    emit idle_timeout(timer_name);
}

bool
idle_filter_t::set_triggers(const QString triggers_str)
{
    json triggers;

    try {
        triggers = json::parse(triggers_str.toStdString());
    } catch (const std::invalid_argument &e) {
        qWarning() << "Failed to parse -" << triggers_str;
        qWarning() << e.what();
        return false;
    }

    clear_triggers();

    for (const json &trigger : triggers) {
        add_trigger(trigger);
    }

    return true;
}

bool
idle_filter_t::clear_triggers()
{
    m_triggers.clear();
    return true;
}

bool
idle_filter_t::add_trigger(json trigger)
{
    if ((trigger.find("disabled") != trigger.end()) && (trigger["disabled"].get<bool>() == true)) {
        return false;
    }

    struct TimerSet timer_set;
    timer_set.mTimerName = QString::fromStdString(trigger["timer-name"]);
    timer_set.mTimeout = trigger["timeout"];

    timer_set.mTimer = std::make_shared<QTimer>();

    // Add a new Timerset to our list
    m_triggers.append(timer_set);

    // Adds a mapping so that whenever timer_set.mTimer signals map(), it will
    // also send the timer name with it.
    connect(timer_set.mTimer.get(), SIGNAL(timeout()), m_signal_mapper.get(), SLOT(map()));
    m_signal_mapper->setMapping(timer_set.mTimer.get(), timer_set.mTimerName);
    if (timer_set.mTimeout > 0) {
        timer_set.mTimer->start(timer_set.mTimeout * 1000);
    }

    return true;
}
