#-------------------------------------------------
#
# Project created by QtCreator 2016-10-22T17:30:34
#
#-------------------------------------------------
TEMPLATE = subdirs

SUBDIRS += xenmgr

toolstack_headers.path = /usr/include/toolstack/include
toolstack_headers.files = \
	include/vm.h \
	include/vm_base.h \
	include/toolstack.h \
	include/qobj.h
INSTALLS += toolstack_headers
