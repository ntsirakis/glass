//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <QDebug>
#include <pv_vm_render_factory.h>

pv_vm_render_factory_t::pv_vm_render_factory_t(renderer_t &renderer) : vm_render_factory_t(renderer)
{
    QObject::connect(this, &pv_vm_render_factory_t::add_guest, &renderer, &renderer_t::add_guest);
}

std::shared_ptr<vm_render_t>
pv_vm_render_factory_t::make_vm_render(toolstack_t &toolstack, std::shared_ptr<vm_base_t> vm)
{
    (void) toolstack;
    auto desktop = m_renderer.desktop(vm->uuid());
    Expects(desktop);

    std::shared_ptr<vm_render_t> vm_render = std::make_shared<pv_vm_render_t>(vm, desktop);

    emit add_guest(vm_render);

    return vm_render;
}
