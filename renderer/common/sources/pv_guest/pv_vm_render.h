//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_VM_RENDER__H
#define PV_VM_RENDER__H

#include <glass_types.h>

#include <vm_render.h>

#include "pv_desktop_resource.h"
#include <QImage>
#include <QObject>

class pv_vm_render_t : public vm_render_t
{
    Q_OBJECT
public:
    pv_vm_render_t(std::shared_ptr<vm_base_t> base, desktop_plane_t *desktop) : vm_render_t(base),
                                                                                m_pv_ready(false),
                                                                                m_desktop(desktop)
    {
        Expects(desktop);
        Expects(base);

        QObject::connect(this, &pv_vm_render_t::teardown_render_targets, desktop, &desktop_plane_t::reset_render_planes, Qt::DirectConnection);

        if (domid_t(base->stub_domid()) < domid_t(DOMID_FIRST_RESERVED)) {
            m_desktops.push_back(std::make_unique<pv_desktop_resource_t>(uuid(),
                                                                         base->stub_domid(),
                                                                         desktop,
                                                                         pv_port::qemu_control_port));

            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);

            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::update_cursor, this, &pv_vm_render_t::show_cursor);
            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::hide_cursor, this, &pv_vm_render_t::hide_cursor);
            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::move_cursor, this, &pv_vm_render_t::move_cursor);
            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::render_source_plane_signal, this, &pv_vm_render_t::render_source_plane_slot);
            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::blank_display_signal, this, &pv_vm_render_t::blank_display_slot);
        }

        if (!desktop->renderable()) {
            return;
        }

        m_desktops.push_back(std::make_unique<pv_desktop_resource_t>(uuid(),
                                                                     base->domid(),
                                                                     desktop,
                                                                     pv_port::pv_control_port));

        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::desktop_ready, this, &pv_vm_render_t::pv_desktop_ready);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::render_source_plane_signal, this, &pv_vm_render_t::render_source_plane_slot);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::blank_display_signal, this, &pv_vm_render_t::blank_display_slot);

        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::update_cursor, this, &pv_vm_render_t::show_cursor);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::hide_cursor, this, &pv_vm_render_t::hide_cursor);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::move_cursor, this, &pv_vm_render_t::move_cursor);
        QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::restore_qemu_signal, this, &pv_vm_render_t::restore_qemu_slot);
    }

    virtual ~pv_vm_render_t()
    {
        emit teardown_render_targets(uuid());
        m_desktops.clear();
    }

signals:
    void teardown_render_targets(uuid_t uuid);

    void calculate_guest_size(uuid_t uuid);

public slots:
    void blank_display_slot(bool isOff)
    {
        m_blanked = isOff;
    }

    void move_cursor(window_key_t key, point_t point)
    {
        emit move_cursor_signal(base()->uuid(), key, point);
    }

    void show_cursor(uint32_t key, point_t point, std::shared_ptr<cursor_t> cursor)
    {
        emit show_cursor_signal(base()->uuid(), key, point, cursor);
    }

    void hide_cursor(uint32_t key)
    {
        emit hide_cursor_signal(base()->uuid(), key);
    }

    void render_source_plane_slot(uint32_t key)
    {
        (void) key;
        emit calculate_guest_size(base()->uuid());
    }

    bool is_qemu_guest(uuid_t uuid)
    {
        for (auto &d : m_desktops) {
            if (!d) {
                continue;
            }

            if (d->uuid() != uuid || d->qemu()) {
                continue;
            }

            if (d->display_info_count() > 0) {
                return false;
            }
        }
        return true;
    }

    void update_render_targets(desktop_plane_t *desktop, uuid_t uuid)
    {
        if (!desktop) {
            m_desktop = nullptr;

            for (auto &d : m_desktops) {
                if (!d) {
                    continue;
                }

                // only update the qemu pv desktop resource or the non-qemu pv desktop resource
                if (d->uuid() == uuid && (is_qemu_guest(uuid) ? d->qemu() : !d->qemu())) {
                    d->update_displays(nullptr);
                }
            }

            return;
        }

        if (desktop != m_desktop || 1) {
            m_desktop = desktop;

            QObject::connect(this, &pv_vm_render_t::teardown_render_targets, m_desktop, &desktop_plane_t::reset_render_planes, Qt::DirectConnection);

            for (auto &d : m_desktops) {
                if (!d) {
                    continue;
                }

                // only update the qemu pv desktop resource or the non-qemu pv desktop resource
                if (d->uuid() == uuid && (is_qemu_guest(uuid) ? d->qemu() : !d->qemu())) {
                    d->update_displays(m_desktop);
                }
            }
        }
    }

    void pv_desktop_ready(bool ready)
    {
        m_pv_ready = ready;

        if (ready) {
            QObject::disconnect(m_desktops.front().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);
            QObject::connect(m_desktops.back().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);
        } else {
            QObject::disconnect(m_desktops.back().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);
            QObject::connect(m_desktops.front().get(), &pv_desktop_resource_t::add_dirty_rect, this, &pv_vm_render_t::add_dirty_rect);

            //Force qemu display on and a full screen redraw
            m_blanked = false;
            if (m_desktop && m_desktop->current_display()) {
                emit dirty_rect(m_desktop->current_display()->rect());
            }
        }

        if (m_desktop) {
            for (auto &render_target : m_desktop->render_targets(uuid())) {
                if (!render_target) {
                    continue;
                }

                render_target->force_qemu_render_source(!ready);
            }
        }
    }

    void add_dirty_rect(rect_t rect)
    {
        emit dirty_rect(rect);
        emit render_signal();
    }

    void restore_qemu_slot()
    {
        emit restore_qemu_signal();
        //un blank the renderer
        m_blanked = false;
    }

private:
    // PV desktop resources (QEmu/PV)
    list_t<std::unique_ptr<pv_desktop_resource_t>> m_desktops;
    std::unique_ptr<QImage> m_icon;

    bool m_pv_ready;
    desktop_plane_t *m_desktop;
};

#endif // PV_VM_RENDER__H
