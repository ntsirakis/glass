#------------------------------------------------
#
# Project created by QtCreator 2016-10-22T17:30:34
#
#-------------------------------------------------
include(common_include.prx)

CONFIG += ordered
TEMPLATE = subdirs

SUBDIRS += common
SUBDIRS += window_manager
SUBDIRS += renderer
SUBDIRS += toolstack
SUBDIRS += input
SUBDIRS += main

main.depends = window_manager renderer input toolstack common
input.depends = toolstack common

QMAKE_CLEAN += $$VG_BASE_DIR/lib/* $$VG_BASE_DIR/bin/glass

common_headers.path = /usr/include/common
common_headers.files = \
	common/include/logging.h \
	common/include/glass_types.h \
	common/include/dbus.h \
	common/include/json.hpp \
	common/include/glass_rect.h \
	common/include/ivc_channel.h
INSTALLS += common_headers
common_gsl_headers.path = /usr/include/common/gsl
common_gsl_headers.files = \
	common/include/gsl/multi_span \
	common/include/gsl/gsl_assert \
	common/include/gsl/gsl_algorithm \
	common/include/gsl/gsl_util \
	common/include/gsl/string_span \
	common/include/gsl/gsl_byte \
	common/include/gsl/span \
	common/include/gsl/gsl
INSTALLS += common_gsl_headers
input_server_config.path = /etc/mosaic
input_server_config.files = configs/inputserver_config
INSTALLS += input_server_config
assets.path = /etc/vglass
assets.files = \
	batteryIcons.png \
	cursor.png
INSTALLS += assets
